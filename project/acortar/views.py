from django.shortcuts import redirect
from django.http import Http404, HttpResponse
from django.urls import NoReverseMatch
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido
import os

def get_counter():
    last_short = Contenido.objects.order_by('-short').first()
    if last_short:
        return int(last_short.short) + 1
    return 1


def get_main(request):
    return render(request, 'acortar/main.html', {'url_acortar': "acortar/"})

@csrf_exempt
def index(request):
    if request.method == "POST":
        url = request.POST.get("url", "").strip()
        if url:
            # Verificar si la URL comienza con http://, https:// o www.
            if not url.startswith(("http://", "https://", "www.")):
                return HttpResponse("La URL debe comenzar por 'http://', 'https://' o 'www.', por favor intentelo de nuevo")

            same_url = Contenido.objects.filter(url__iexact=url).exists()
            if not same_url:
                if not request.POST.get("short"):
                    short = str(get_counter())
                else:
                    short = request.POST["short"]
                Contenido.objects.create(url=url, short=short)

    content_list = Contenido.objects.all()
    return render(request, 'acortar/inicio.html', {'content_list': content_list})

@csrf_exempt
def get_content(request, llave):

    try:
        contenido = Contenido.objects.get(short=llave)
        return redirect(contenido.url)

    except Contenido.DoesNotExist:
        raise Http404("El recurso " + llave + "/ no existe")

    except NoReverseMatch:
        raise Http404("La URL: " + contenido.url + " no es valida")


def get_favicon(request):
    favicon_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static', 'favicon.ico')

    try:
        with open(favicon_path, "rb") as f:
            favicon = f.read()
        return HttpResponse(favicon, content_type="image/x-icon")
    except FileNotFoundError:
        return HttpResponse(status=404)
