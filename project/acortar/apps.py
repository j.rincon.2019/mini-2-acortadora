from django.apps import AppConfig

class acortarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'acortar'
