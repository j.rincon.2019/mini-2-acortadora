from django.db import models

# Create your models here.

class Contenido(models.Model):
    url = models.CharField(max_length=64)
    short = models.TextField()

    def __str__(self):
        return str(self.id) + ": " + self.url + " --- " + self.short